<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/sieraden', 'ItemController@index')->name('sieraden.index');
Route::get('/sieraden/{slug}', 'ItemController@show')->name('sieraden.show');

Route::get('/blogs', 'BlogController@index')->name('blogs.index');
Route::get('/blogs/{slug}', 'BlogController@show')->name('blogs.show');

Route::get('/cart', 'CartController@index')->name('cart.index');
Route::post('/cart', 'CartController@store')->name('cart.store');
Route::post('/cart/remove', 'CartController@remove')->name('cart.remove');


if (!Request::is('rbb-admin'))
{
    Route::get('{slug}', 'PageController@show');
}

Route::group(['prefix' => 'rbb-admin'], function () {
    Voyager::routes();
});

Auth::routes();
