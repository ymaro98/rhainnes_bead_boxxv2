<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>window.laravel = {csrfToken: '{{ csrf_token() }}'}</script>

    <title>{{ setting('site.title') }} | @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Extra styles -->
    @yield('extra_styles')

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('storage/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('storage/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('storage/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('storage/favicon/site.webmanifest') }}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
    <div id="app">
        @if(setting('site.announcement_on_off') == 1)
            <div class="top-alert row justify-content-center">
                <h5 class="top-alert-text">
                    {{ setting('site.announcement_bar_text') }}
                </h5>
            </div>
        @endif

        <header id="header">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <div class="nav-image brand-logo">
                        <img id="logo" class="logo-image" src="{{ asset('storage/logos/rbb_banner_trp_small.png') }}" alt="{{ config('app.name', 'Laravel') }}">
                    </div>
                </a>
                <div class="right-links">
                    <ul>
                        <nav-cart cart-route="{{ route('cart.index') }}" cart-count="{{ Cart::count() }}"></nav-cart>
                        <li><a href="#"><span class="ico-account"></span>Account</a></li>
                        <li><a href="#"><span class="ico-signout"></span>Sign out</a></li>
                    </ul>
                </div>
            </div>
        </header>

        <nav id="menu">
            <div class="container">
                <div class="trigger"></div>
                <ul class>
                    {{ menu('main', 'partials.menus.main_menu') }}
                </ul>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <!-- Extra Scripts -->
    @yield('extra_js')
</body>
</html>
