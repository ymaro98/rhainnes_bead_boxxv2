@extends('layouts.app')

@section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)

@section('content')

    <div id="slider">
        <home-carousel slider-items="{{ $sliders }}"></home-carousel>
        {{-- <div class="home-slide" style="background-image: url('http://beadwear.test//storage/home-sliders/April2020/pITKlDgmX7paWVpSp3pV.jpg')">
            <h2><span>g43gh35tg54g45g</span></h2>
            <h3><span>ghtwt4q3hw435qwy4gv</span></h3>
            <a href="${slider.button_link}" class="btn-more">${slider.button_text}</a>
        </div> --}}
    </div>

    <div id="body">

        <div class="container">

            <div class="last-products">
                <h2 class="text-center">Nieuwste Sieraden</h2>
                <section class="products">
                    <home-items latest-items="{{ $items }}" post-url="{{ route('cart.store') }}"></home-items>
                </section>
            </div>

            <section class="quick-links">
                <article style="background-image: url(images/2.jpg)">
                    <a href="#" class="table">
                        <div class="cell">
                            <div class="text">
                                <h4>Lorem ipsum</h4>
                                <hr>
                                <h3>Dolor sit amet</h3>
                            </div>
                        </div>
                    </a>
                </article>
                <article class="red" style="background-image: url(images/3.jpg)">
                    <a href="#" class="table">
                        <div class="cell">
                            <div class="text">
                                <h4>consequatur</h4>
                                <hr>
                                <h3>voluptatem</h3>
                                <hr>
                                <p>Accusantium</p>
                            </div>
                        </div>
                    </a>
                </article>
                <article style="background-image: url(images/4.jpg)">
                    <a href="#" class="table">
                        <div class="cell">
                            <div class="text">
                                <h4>culpa qui officia</h4>
                                <hr>
                                <h3>magnam aliquam</h3>
                            </div>
                        </div>
                    </a>
                </article>
            </section>
        </div>
    </div>
@endsection
