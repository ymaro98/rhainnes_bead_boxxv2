@extends('layouts.app')

{{-- @section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords) --}}

@section('content')
    <div id="body">
        <div class="container">
            <div id="content" class="full">
                <div class="cart-table">
                    @if(Cart::count() > 0)
                        <h4 id="cart-table-info"></h4>
                        <table>
                            <tr>
                                <th class="items">Items</th>
                                <th class="price">Prijs</th>
                                <th class="qnt">Stuks</th>
                                <th class="total">Totaal</th>
                                <th class="delete"></th>
                            </tr>
                            @foreach (Cart::content() as $item)
                                <tr id="item-{{ $item->rowId }}">
                                    <td class="items">
                                        <div class="image">
                                            <img src="images/6.jpg" alt="">
                                        </div>
                                        <h3><a href="{{ route('sieraden.show', ['slug' => $item->model->name_slug]) }}">{{ $item->model->name }}</a></h3>
                                        <p>{{ $item->model->description }}</p>
                                    </td>
                                    <td class="price">{{ number_format($item->price, 2) }}</td>
                                    <td class="qnt text-center">{{ $item->qty }} {{ ($item->qty > 1) ? 'Stuks' : 'Stuk' }}</td>
                                    <td class="total">{{ number_format($item->price * $item->qty, 2) }}</td>
                                    <td>
                                        <remove-from-cart-sm
                                            post-url="{{ route('cart.remove') }}"
                                            row-id="{{ $item->rowId }}"
                                        ></remove-from-cart-sm>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <h3 class="text-center">Op dit moment zijn er geen items in uw shoppingcart</h3>
                        <h4 class="text-center">Bekijk de <span ><a class="text-info" href="{{ route('sieraden.index') }}">sieraden pagina</a></span> om items toe te voegen</h4>
                    @endif
                </div>
                @if(Cart::count() > 0)

                    <total-count-cart
                        sub-total="{{ Cart::subtotal() }}"
                        shippment="{{ 0 }}"
                        btw="{{ Cart::tax() }}"
                        cart-total="{{ Cart::total() }}"
                    ></total-count-cart>
                @endif
            </div>
            <!-- / content -->
        </div>
        <!-- / container -->
    </div>
    <!-- / body -->
@endsection
