@foreach ($items as $item)
    <p>
        <a href="{{ url($item->link()) }}" target="{{ $item->target }}">
            <span>{{ $item->title }}</span>
        </a>
    </p>
@endforeach