@extends('layouts.app')

@section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)
@section('extra_styles')
<link href="{{ asset('css/clean-blog.css') }}" rel="stylesheet">
@endsection

@section('content')
<header class="masthead" style="background-image: url('{{ Voyager::image($blog->thumbnail) }}')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-heading">
                    <h1>{{ $blog->title }}</h1>
                    <h2 class="subheading">{{ $blog->intro }}</h2>
                    <span class="meta">Gepost door
                        <a href="#">{{ $blog->user->name }}</a>
                        op {{ $blog->created_at->format('l jS \\, F Y h:i:s A') }}
                    </span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Post Content -->
<article>
    <div class="container">
        {!! $blog->body !!}
    </div>
</article>
@endsection
