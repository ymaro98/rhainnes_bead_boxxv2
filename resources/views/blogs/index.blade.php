@extends('layouts.app')

@section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)
@section('extra_styles')
<link href="{{ asset('css/clean-blog.css') }}" rel="stylesheet">
@endsection

@section('content')

<header class="masthead" style="background-image: url('{{ Voyager::image($page->image) }}')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>{{ $page->title }}</h1>
                    <span class="subheading">{{ $page->intro }}</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            @foreach ($blogs as $blog)

                <div class="post-preview">
                    <a href="{{ route('blogs.show', ["slug" => $blog->title_slug]) }}">
                        <h2 class="post-title">
                            {{ $blog->title }}
                        </h2>
                        <h3 class="post-subtitle">
                            {{ $blog->intro }}
                        </h3>
                    </a>
                    <p class="post-meta">Gepost door
                        <a href="#">{{ $blog->user->name }}</a>
                        {{-- on {{ $blog->created_at->toFormattedDateString() }} --}}
                        op {{ $blog->created_at->format('l jS \\, F Y h:i:s A') }}
                    </p>
                </div>
                <hr>
            @endforeach

            <!-- Pager -->
            <div class="clearfix">
                @if($blogs->currentPage() != $blogs->onFirstPage())
                <a class="btn btn-primary float-left paginate-old-new" href="{{ $blogs->previousPageUrl() }}">&larr; Nieuwere Blogs</a>
                @endif

                @if($blogs->currentPage() != $blogs->lastPage())
                <a class="btn btn-primary float-right paginate-old-new" href="{{ $blogs->nextPageUrl() }}">Oudere Blogs &rarr;</a>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
