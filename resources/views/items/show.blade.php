@extends('layouts.app')

@section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)

@section('content')
    <div class="container">
        <div id="content" class="full">
            <div class="product">

                <item-images item-images="{{ $images }}"></item-images>

                <div class="details">
                    <h1>{{ $item->name }}</h1>
                    <h4>{{ $item->price }}</h4>
                    <div class="entry">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                        <div class="tabs">
                            <div class="nav">
                                <ul>
                                    <li class="active"><a href="#desc">Description</a></li>
                                    <li><a href="#spec">Specification</a></li>
                                    <li><a href="#ret">Returns</a></li>
                                </ul>
                            </div>
                            <div class="tab-content active" id="desc">
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                            </div>
                            <div class="tab-content" id="spec">
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                            </div>
                            <div class="tab-content" id="ret">
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                            </div>
                        </div>
                    </div>
                    <div class="actions">
                        @if($item->inCart == true)
                            <remove-from-cart
                                post-url="{{ route('cart.remove') }}"
                                row-id="{{ $item->rowId }}"
                                button-text="Uit cart verwijderen"
                                button-css="btn-block btn-grey text-center remove-item"
                            ></remove-from-cart>
                        @else
                            <add-to-cart
                                item-id="{{ $item->id }}"
                                item-name="{{ $item->name }}"
                                item-price="{{ $item->price }}"
                                post-url="{{ route('cart.store') }}"
                                button-text="Add to cart"
                                button-css="btn-block btn-grey text-center"
                            ></add-to-cart>
                        @endif
                        <cart-message></cart-message>
                    </div>
                </div>
            </div>
        </div>
        <!-- / content -->
    </div>
    <!-- / container -->
@endsection
