@extends('layouts.app')

@section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)

@section('content')

    <div class="container">
        <items-index></items-index>
        <cart-message></cart-message>
    </div>
@endsection
