/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./main');
require('./plugins');
require('./smoothScroll.js');

window.Vue = require('vue');
export const bus = new Vue();

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { InputNumber, Checkbox, CheckboxGroup } from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

library.add([faChevronUp, faChevronDown]);
Vue.component(InputNumber.name, InputNumber);
Vue.component(Checkbox.name, Checkbox);
Vue.component(CheckboxGroup.name, CheckboxGroup);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('add-to-cart', require('./components/cart/AddToCart.vue').default);
Vue.component('remove-from-cart-sm', require('./components/cart/RemoveFromCartSmall.vue').default);
Vue.component('remove-from-cart', require('./components/cart/RemoveFromCart.vue').default);
Vue.component('cart-message', require('./components/cart/CartMessage.vue').default);
Vue.component('nav-cart', require('./components/cart/NavCart.vue').default);
Vue.component('total-count-cart', require('./components/cart/TotalCountCart.vue').default);

Vue.component('home-carousel', require('./components/carousels/HomeCarousel.vue').default);
Vue.component('home-items', require('./components/carousels/HomeLatestItems.vue').default);
Vue.component('items-index', require('./components/items/Items.vue').default);
Vue.component('item-images', require('./components/items/ItemPageImages.vue').default);

Vue.component('category-filter', require('./components/sidebarFilters/Category.vue').default);
Vue.component('color-filter', require('./components/sidebarFilters/Color.vue').default);

Vue.component('font-awesome-icon', FontAwesomeIcon);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
