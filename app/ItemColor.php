<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemColor extends Model
{
    protected $fillable = [
        'item_id',
        'color_id',
    ];

    public function color()
    {
        return $this->belongsTo('App\Color');
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public static function colorsWithNameSlug($colors)
    {
        foreach ($colors as $color) {
            $color->name = $color->color->name;
            $color->name_slug = $color->color->name_slug;
        }

        return $colors;
    }
}
