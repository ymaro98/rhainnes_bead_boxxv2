<?php

namespace App;

use Gloudemans\Shoppingcart\Contracts\Buyable;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Database\Eloquent\Model;

class Item extends Model implements Buyable
{
    protected $fillable = [
        'name',
        'name_slug',
        'category_id',
        'description',
        'meta_keywords',
        'meta_description',
        'price',
        'stock',
        'images',
        'color_id',
    ];

    public function getBuyableIdentifier($options = null) {
        return $this->id;
    }
    public function getBuyableDescription($options = null) {
        return $this->name;
    }
    public function getBuyablePrice($options = null) {
        return $this->price;
    }
    public function getBuyableWeight($options = null) {
        return $this->weight;
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function colors()
    {
        return $this->hasMany('App\ItemColor');
    }

    public function scopeFindBySlug($query, $slug)
    {
        return $query->where('name_slug', $slug);
    }

    public static function inCart($item)
    {
        if(Cart::count() > 0){

            $duplicates = Cart::search(function($cartItem) use ($item){
                return $cartItem->id == $item->id;
            });

            if($duplicates->isNotEmpty()){
                return $duplicates;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
