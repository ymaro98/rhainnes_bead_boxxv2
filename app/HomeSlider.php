<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeSlider extends Model
{
    protected $fillable = [
        'name',
        'sub_text',
        'big_text',
        'button_text',
        'button_link',
        'image',
        'color_glow',
    ];
}
