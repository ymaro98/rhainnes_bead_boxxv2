<?php

namespace App\Http\Controllers;

use App\HomeSlider;
use App\Item;
use App\Page;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $page = Page::findBySlug('home')->firstOrFail();
        $sliders = HomeSlider::all();
        $last_products = Item::with('colors')->limit(12)->latest()->get();

        $collection = collect($sliders);
        $newSliders = $collection->map(function ($slider) {
            $slider->image = Voyager::image($slider->image);
            return $slider;
        });

        $sliders = $newSliders;
        return view('home', [
            'page' => $page,
            'sliders' => $sliders,
            'items' => $last_products,
        ]);
    }
}
