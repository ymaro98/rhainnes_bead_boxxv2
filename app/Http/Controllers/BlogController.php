<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Jsonable;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Page::findBySlug('blogs')->firstOrFail();
        $blogs = Blog::latest()->paginate();

        return view('blogs.index', [
            'page' => $page,
            'blogs' => $blogs,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog, $slug)
    {
        $page = Page::findBySlug('blog')->first();
        $blog = Blog::findBySlug($slug)->first();

        return view('blogs.show', [
            'page' => $page,
            'blog' => $blog,
        ]);
    }
}
