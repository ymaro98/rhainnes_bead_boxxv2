<?php

namespace App\Http\Controllers;

use App\Category;
use App\Color;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function categories()
    {
        $categories = Category::all();
        return $categories;
    }

    public function colors()
    {
        $colors = Color::all();
        return $colors;
    }

    public function fetchItemFilters()
    {
        $categories = $this->categories();
        $colors = $this->colors();

        return response()->json(array(
            'categories' => $categories,
            'colors' => $colors,
        ));
    }
}
