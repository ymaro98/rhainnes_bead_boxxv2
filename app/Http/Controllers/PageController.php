<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function show($slug)
    {
        $page = Page::findBySlug($slug)->firstOrFail();
        if(view()->exists($slug)){
            $view = view($slug, ['page' => $page]);
        } else {
            $view = abort(404);
        }
        return $view;
    }
}
