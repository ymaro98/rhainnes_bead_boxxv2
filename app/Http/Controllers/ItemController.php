<?php

namespace App\Http\Controllers;

use App\Http\Resources\ItemResource;
use App\Item;
use App\Page;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Page::findBySlug('sieraden')->firstOrFail();

        return view('items.index', [
            'page' => $page,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item, $slug)
    {
        $page = Page::findBySlug('sieraad')->first();
        $item = Item::findBySlug($slug)->firstOrFail();

        $duplicate = Item::inCart($item);
        if($duplicate){
            $item->inCart = true;
            $item->rowId = $duplicate->first()->rowId;
        } else {
            $item->inCart = false;
        }

        $images = json_decode($item->images);
        $collection = collect($images);
        $newImages = $collection->map(function ($image) {
            $image = Voyager::image($image);
            return $image;
        });
        $images = $newImages;

        return view('items.show', [
            'page' => $page,
            'item' => $item,
            'images' => $images,
        ]);
    }

    public function itemsApi(Request $request)
    {
        $items_q = Item::with(['category', 'colors']);
        $offset = (int)$request->get('offset');
        $searchFilter = $request->get('search');
        $categoryFilter = $request->get('categoryChecks');
        $colorFilter = $request->get('colorChecks');


        if ($searchFilter !== null) {
            $items_q->where('name', 'like', '%' . $searchFilter . '%');
        }

        if ($categoryFilter !== null) {

            $categories = explode(',', $categoryFilter);

            $items_q->whereHas("category", function($q) use ($categories){
                $q->whereIn('name_slug', $categories);
            })->first();
        }

        if ($colorFilter !== null) {

            $colors = explode(',', $colorFilter);

            $items_q->whereHas("colors.color", function($q) use ($colors){
                $q->whereIn('name_slug', $colors);
            })->first();
        }

        $items = $items_q->skip($offset)->take(24)->get();
        return ItemResource::collection($items);
    }
}
