<?php

namespace App\Http\Resources;

use App\Item;
use App\ItemColor;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'prod_item' => $this->id,
            'name' => $this->name,
            'name_slug' => $this->name_slug,
            'description' => $this->description,
            'images' => json_decode($this->images),
            'price' => $this->price,
            'category' => $this->category->name_slug,
            'colors' => ItemColor::colorsWithNameSlug($this->colors),
        ];
    }
}
