<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    protected $fillable = ['name', 'path'];

    public function url()
    {
        return Storage::url($this->path);
    }
}
