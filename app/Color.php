<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = [
        'name',
        'name_slug',
        'color_code',
    ];

    public function items()
    {
        return $this->hasMany('App\Item');
    }
}
