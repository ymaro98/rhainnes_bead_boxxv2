<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use Faker\Generator as Faker;
use Carbon\Carbon;


$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(10),
        'title_slug' => $faker->sentence(10),
        'intro' => $faker->sentence(1),
        'thumbnail' => $faker->sentence(1),
        'body' => $faker->paragraphs(5, true),
        'created_at' => $faker->dateTimeBetween('-3 months'),
    ];
});

$factory->state(App\Blog::class, 'test-post', function (Faker $faker){
    return [
        'title' => 'Testing Admin Blog',
        'content' => 'Content of the Admin Blog',
        'user_id' => 1,
        'created_at' => Carbon::now()->toDateTimeString(),
    ];
});

