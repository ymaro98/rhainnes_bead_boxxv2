<?php

use App\Blog;
use App\User;
use Illuminate\Database\Seeder;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $blogCount = (int) $this->command->ask('How many posts would you like?', 50);
        $users = User::all();

        factory(Blog::class, $blogCount)->make()->each(function($post) use ($users){
            $post->author_id = $users->random()->id;
            $post->save();
        });
        // factory(Blog::class)->states('test-post')->create();
    }
}
