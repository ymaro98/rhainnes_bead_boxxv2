<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('title');
            $table->string('title_slug');
            $table->string('intro');
            $table->string('thumbnail');
            $table->string('body');
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('blog_keywords')->nullable();
            $table->string('blog_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
